<%@include file="/libs/foundation/global.jsp"%>
<% %>
<%@page import="java.util.Iterator, com.day.cq.wcm.api.PageFilter"%>

<%
	String listroot = properties.get("listroot",currentPage.getPath());
	String url_site = properties.get("url",currentPage.getPath());

	Page rootPage = pageManager.getPage(listroot);

    if(rootPage != null){
        Iterator<Page> children = rootPage.listChildren(new PageFilter(request));
        while(children.hasNext()){
			Page child = children.next();
            String title = child.getTitle() == null ? child.getName() : child.getTitle();
            String date = child.getProperties().get("date","");%>
			<div class="item">
				<a href="<%=child.getPath()%>.html">
                    <b><%=title %></b>
                </a>
                <span>
					<%=date %><br>
                </span>
                <%=child.getProperties().get("jcr:description","")%><br>
                <p>Vanity URL: <%=child.getProperties().get("sling:vanityPath","")%></p>(<%=url_site %>)<br>

			</div>
<%
        }
    }
%>